package com.ag04.common.web.domain.dto;

import java.io.Serializable;

/**
 * Dto representing Entity Conflict error most probably caused by optimistic/pesimistic locking.
 *
 * Created by dmadunic on 29/03/16.
 */
public class EntityConflictError extends EntityError {

    Long version;
    Long currentVersion;
    String lastModifier;

    Object entity;

    public EntityConflictError() {
        //
    }

    public EntityConflictError(Long version, Long currentVersion, String lastModifier, Object entity) {
        super();
        this.version = version;
        this.currentVersion = currentVersion;
        this.lastModifier = lastModifier;
        this.entity = entity;
    }

    //--- set / get methods ---------------------------------------------------

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public Long getCurrentVersion() { return currentVersion; }

    public void setCurrentVersion(Long currentVersion) { this.currentVersion = currentVersion; }

    public Long getVersion() { return version; }

    public void setVersion(Long version) { this.version = version; }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

}
