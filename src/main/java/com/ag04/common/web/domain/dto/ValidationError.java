package com.ag04.common.web.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by: dmadunic on 25/12/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidationError extends BaseError {

    List<BaseError> globalErrors;

    List<FieldError> fieldErrors;

    //--- helper methods ------------------------------------------------------

    public void addGlobalError(BaseError error) {
        if (globalErrors == null) {
            globalErrors = new ArrayList<BaseError>();
        }
        globalErrors.add(error);
    }

    public void addFieldError(FieldError error) {
        if (fieldErrors == null) {
            fieldErrors = new ArrayList<FieldError>();
        }
        fieldErrors.add(error);
    }

    //--- set / get methods ---------------------------------------------------

    public List<BaseError> getGlobalErrors() {
        return globalErrors;
    }

    public void setGlobalErrors(List<BaseError> globalErrors) {
        this.globalErrors = globalErrors;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldError> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
