package com.ag04.common.web.domain.dto;

import com.fasterxml.jackson.databind.deser.Deserializers;

import java.util.ArrayList;
import java.util.List;

/**
 * s
 * Created by dmadunic on 29/03/16.
 */
public class MultipleErrorsResponse extends BaseError {

    private List<BaseError> errors;

    public MultipleErrorsResponse(final String code, final String message) {
        super(code, message);
    }

    public void addError(BaseError error) {
        if (errors == null) {
            errors = new ArrayList<BaseError>();
        }
        errors.add(error);
    }

    public List<BaseError> getErrors() {
        return errors;
    }

    public void setErrors(List<BaseError> errors) {
        this.errors = errors;
    }
}
