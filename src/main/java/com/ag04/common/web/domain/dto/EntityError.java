package com.ag04.common.web.domain.dto;

/**
 * Created by dmadunic on 29/03/16.
 */
public class EntityError extends BaseError {

    private String entityId;

    public EntityError() {
        //
    }

    public EntityError(final String code, final String message, final String entityId) {
        super(code, message);
        this.entityId = entityId;
    }

    //--- set / get methods ---------------------------------------------------

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
