package com.ag04.common.web.domain.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dmadunic on 29/03/16.
 */
public class BaseError implements Serializable {

    private String code;
    private String message;

    public BaseError() {
        //
    }

    public BaseError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    //--- set / get methods ---------------------------------------------------

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
