package com.ag04.common.web.domain.dto;

import java.io.Serializable;

/**
 * Created by dmadunic on 29/03/16.
 */
public class FieldError extends BaseError {

    private String field;
    private Object value;

    public FieldError(final String code, final String message, final Object value, final String field) {
        super(code, message);
        this.field = field;
        this.value = value;
    }

    //--- set / get methods ---------------------------------------------------

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
