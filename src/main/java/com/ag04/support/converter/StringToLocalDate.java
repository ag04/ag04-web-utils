package com.ag04.support.converter;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;
/**
 * 
 * @author dmadunic
 *
 */
public class StringToLocalDate implements Converter<String, LocalDate> {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    
    @Override
    public LocalDate convert(String value) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT); 
        return formatter.parseLocalDate(value);
    }

}
