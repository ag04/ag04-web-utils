package com.ag04.support.jackson;

import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat;
import com.fasterxml.jackson.datatype.joda.deser.DateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;
import com.fasterxml.jackson.datatype.joda.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.joda.ser.LocalDateTimeSerializer;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.datetime.joda.DateTimeFormatterFactory;

public class JodaModulConfigurator {

    public static JodaModule jacksonJodaModule() {
        JodaModule module = new JodaModule();

        //--- LocalDate --------------------------
        DateTimeFormatterFactory localDateFormatterFactory = new DateTimeFormatterFactory();
        // yyyy-MM-dd
        localDateFormatterFactory.setIso(DateTimeFormat.ISO.DATE);

        module.addSerializer(LocalDate.class, new LocalDateSerializer(new JacksonJodaDateFormat(localDateFormatterFactory.createDateTimeFormatter())));
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer(new JacksonJodaDateFormat(localDateFormatterFactory.createDateTimeFormatter())));

        //--- LocalDateTime ----------------------

        DateTimeFormatterFactory dateTimeFormatterFactory = new DateTimeFormatterFactory();
        // yyyy-MM-dd'T'HH:mm:ss.SSSZ
        dateTimeFormatterFactory.setIso(DateTimeFormat.ISO.DATE_TIME);

        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(new JacksonJodaDateFormat(dateTimeFormatterFactory.createDateTimeFormatter())));
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(new JacksonJodaDateFormat(dateTimeFormatterFactory.createDateTimeFormatter())));

        // DateTime ...
        module.addSerializer(DateTime.class, new DateTimeSerializer(new JacksonJodaDateFormat(dateTimeFormatterFactory.createDateTimeFormatter())));
        module.addDeserializer(DateTime.class, DateTimeDeserializer.forType(DateTime.class));

        //TODO: add other date / datetime formats here ...

        return module;
    }
}
