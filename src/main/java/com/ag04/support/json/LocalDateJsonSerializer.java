package com.ag04.support.json;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.io.IOException;

/**
 * @author hilic
 */
public class LocalDateJsonSerializer extends StdSerializer<LocalDate> {
	
	private static final String DATE_FORMAT = "YYYY-MM-dd";

	private DateTimeFormatter formatter;

	public LocalDateJsonSerializer() {
		super(LocalDate.class);
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		formatter = DateTimeFormat.forPattern(DATE_FORMAT);
	}

	@Override
	public void serialize(LocalDate value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
		jgen.writeString(value.toString(formatter));
	}
}
