package com.ag04.support.json;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * 
 * @author dmadunic, arados
 *
 */
public class LocalDateJsonDeserializer extends StdDeserializer<LocalDate> {
	private static final String DATE_FORMAT = "YYYY-MM-dd";
	
	private DateTimeFormatter formatter;
	
	public LocalDateJsonDeserializer() {
		super(LocalDate.class);
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		formatter = DateTimeFormat.forPattern(DATE_FORMAT);
	}

	@Override
	public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		JsonToken t = jp.getCurrentToken();
		if (t != JsonToken.VALUE_STRING) {
			throw ctxt.wrongTokenException(jp, jp.getCurrentToken(), "Token must be a string.");
		}
		
		String rawToken = jp.getText().trim();
		if (rawToken.isEmpty()) {
			return null;
		}
		try {
			return LocalDate.parse(rawToken, formatter);
		} catch (IllegalArgumentException e) {
			throw ctxt.wrongTokenException(jp, jp.getCurrentToken(), "Token must be date in format '" + formatter + "'");
		}
	}

}
