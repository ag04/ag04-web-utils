package com.ag04.support.security.userdetails;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

/**
 * 
 * @author dmadunic
 *
 */
public class SimpleJdbcDaoImpl extends JdbcDaoImpl {

	private String defaultRole = null;
	
	protected void addCustomAuthorities(String username, List<GrantedAuthority> authorities) {
		if (defaultRole != null) {
			authorities.add(new SimpleGrantedAuthority(defaultRole));
		}
	}
	//--- set / get authorities -----------------------------------------------
	
	public String getDefaultRole() {
		return defaultRole;
	}

	public void setDefaultRole(String defaultRole) {
		this.defaultRole = defaultRole;
	}
}
