package com.ag04.support.security.rest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

/**
 * 
 * @author dmadunic
 *
 */
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	/**
	 * URL to which client should be redirected in case of remember me authentication.
	 * 
	 */
	private String redirectUrl = "/";
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	 
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
    	logger.info("--> Authentication=" + authentication);
    	if (authentication instanceof RememberMeAuthenticationToken) {
    		logger.info("-----> Returning: redirect to '" + redirectUrl + "'");
    		redirectStrategy.sendRedirect(request, response, redirectUrl);
    	} else {
    		logger.info("-----> Returning: HTTP STATUS 200 - OK");
        	response.setStatus(HttpServletResponse.SC_OK);
    	}
    	clearAuthenticationAttributes(request);
    }
    
    //--- set / get methods ---------------------------------------------------

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
    
}
