package com.ag04.utils.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * Created by dmadunic on 27/03/16.
 */
public class ManifestUtil {
    private static final Logger LOG = LoggerFactory.getLogger(ManifestUtil.class);

    private static final String MAINFEST_PATH = "/META-INF/MANIFEST.MF";

    public static String getProperty(final ServletContext p_servletContext, final String p_propertyName) {
        final Properties prop = readManifest(p_servletContext, MAINFEST_PATH);
        final String value = prop.getProperty(p_propertyName);

        return value;
    }

    public static Properties readManifest(final ServletContext p_servletContext, final String p_resourcePath) {
        Properties prop = new Properties();
        final InputStream manifestStream = p_servletContext.getResourceAsStream(p_resourcePath);
        if (manifestStream == null) {
            LOG.warn("Failed reading resource from path='{}' ---> returning empty Properties map", p_resourcePath);
            return prop;
        }

        try {
            prop.load(manifestStream);
            return prop;
        } catch (IOException e) {
            LOG.error("FAILED to load properties from path='" + p_resourcePath + "' ---> returning empty Properties map", e);
            return prop;
        } finally {
            try {
                manifestStream.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}