package com.ag04.utils.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Manifest;

/**
 *  <h2>Configuration:</h2> 
 *  <pre> 
 *   private ServletContext servletContext;
 *
 *   public ManifestReader manifestReader() {
 *       ManifestReader manifestReader = new ManifestReader();
 *       manifestReader.setServletContext(servletContext);
 *       return manifestReader;
 *   }
 *  </pre> 
 *  <h2>Usage:</h2>
 *  <pre>  
 *  manifestReader.getBuildVersion()
 *  manifestReader.getBuildTime()
 *  </pre>
 *
 * Created by lovrostrihic on 11.3.2016..
 */
public class ManifestReader {
    private static final Logger LOG = LoggerFactory.getLogger(ManifestReader.class);

    private static final String MANIFEST_LOCATION = "/META-INF/MANIFEST.MF";

    private static final String MANIFEST_BUILD_VERSION_KEY = "Build-Version";
    private static final String MANIFEST_BUILD_TIME_KEY = "Build-Time";

    private Manifest manifest;

    private ServletContext servletContext;

    @PostConstruct
    private void postConstruct() {
        manifest = getWarManifest();
    }

    public String getBuildVersion() {
        String buildVersion;

        if (manifest == null) {
            buildVersion = "no MANIFEST file found";
        } else {
            buildVersion = manifest.getMainAttributes().getValue(MANIFEST_BUILD_VERSION_KEY);
        }

        return buildVersion;
    }


    public String getBuildTime() {
        String buildTime;

        if (manifest == null) {
            buildTime = "no MANIFEST file found";
        } else {
            buildTime = manifest.getMainAttributes().getValue(MANIFEST_BUILD_TIME_KEY);
        }

        return buildTime;
    }

    /**
     * Read war manifest data.
     *
     * @return war <tt>Manifest</tt>.
     */
    private Manifest getWarManifest() {
        Manifest manifest = null;
        try {
            InputStream input = servletContext.getResourceAsStream(MANIFEST_LOCATION);
            manifest = new Manifest(input);
        } catch (IOException | NullPointerException e) {
            LOG.error("Can't read MANIFEST.MF file. -----> {}", e.getMessage());
        }
        return manifest;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
