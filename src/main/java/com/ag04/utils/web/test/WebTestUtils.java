package com.ag04.utils.web.test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmadunic on 27/03/16.
 */
public class WebTestUtils {
    private static final Map<String, Object> REQUEST_MAP = new HashMap<String, Object>();

    private WebTestUtils() {
        //
    }

    /**
     * Sets up a mock web request context, to be used for tests outside web
     * application server if needed.
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest setupMockWebRequestContext() {

        final MockServletContext servletContext = new MockServletContext();
        final WebApplicationContext webApplicationContext = mock(WebApplicationContext.class);
        final MessageSource mockMessageSource = mock(MessageSource.class);
        when(webApplicationContext.getBean(eq("messageSource"))).thenReturn(mockMessageSource);

        servletContext.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, webApplicationContext);
        final MockHttpServletRequest mockRequest = new MockHttpServletRequest(servletContext);
        mockRequest.setSession(new MockHttpSession(servletContext));

        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(mockRequest) {

            @Override
            public void setAttribute(final String p_name, final Object p_value, final int p_scope) {
                WebTestUtils.REQUEST_MAP.put(p_name, p_value);
            }

            @Override
            public Object resolveReference(final String p_key) {
                return null;
            }

            @Override
            public void removeAttribute(final String p_name, final int p_scope) {
                WebTestUtils.REQUEST_MAP.remove(p_name);
            }

            @Override
            public void registerDestructionCallback(final String p_name, final Runnable p_callback, final int p_scope) {
                // mock method
            }

            @Override
            public Object getSessionMutex() {
                return null;
            }

            @Override
            public String getSessionId() {
                return null;
            }

            @Override
            public String[] getAttributeNames(final int p_scope) {
                return WebTestUtils.REQUEST_MAP.keySet().toArray(new String[] {});
            }

            @Override
            public Object getAttribute(final String p_name, final int p_scope) {
                return WebTestUtils.REQUEST_MAP.get(p_name);
            }
        });
        return mockRequest;
    }

    public static class TestHttpSession implements HttpSession {

        private final Map<String, Object> attrs = new HashMap<String, Object>();

        public void setMaxInactiveInterval(final int p_interval) {
            //nothing to do
        }

        public void setAttribute(final String p_name, final Object p_value) {
            attrs.put(p_name, p_value);
        }

        public void removeValue(final String p_name) {
            attrs.remove(p_name);
        }

        public void removeAttribute(final String p_name) {
            attrs.remove(p_name);
        }

        public void putValue(final String p_name, final Object p_value) {
            attrs.put(p_name, p_value);
        }

        public boolean isNew() {
            return false;
        }

        public void invalidate() {
            attrs.clear();
        }

        public String[] getValueNames() {
            return attrs.keySet().toArray(new String[attrs.size()]);
        }

        public Object getValue(final String p_name) {
            return attrs.get(p_name);
        }

        public HttpSessionContext getSessionContext() {
            return null;
        }

        public ServletContext getServletContext() {
            return null;
        }

        public int getMaxInactiveInterval() {
            return 0;
        }

        public long getLastAccessedTime() {
            return 0;
        }

        public String getId() {
            return null;
        }

        public long getCreationTime() {
            return 0;
        }

        public Enumeration<String> getAttributeNames() {
            return null;
        }

        public Object getAttribute(final String p_name) {
            return null;
        }
    };
}
