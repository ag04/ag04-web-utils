# ag04-web-utils

Library for various AG04 web utils: 
* Error Dto's hierarchy, 
* Jackson standard Joda time serializer/deseializers, 
* Spring joda time converter, 
* Manifest reader ...

## Usage
### Requirements
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Maven](https://maven.apache.org/download.cgi)

### Setup (First time)
1. Clone the repository: `git clone https://YoutUserName@bitbucket.org/ag04/ag04-web-utils.git`
4. Build project with: ` mvn clean install `

### Release
1) Commit everything
2) mvn release:prepare
3) mvn release:perform


## Changelog

### Version 1.0.13
* Cleaning up removed **EntityMapper<E,S>** interface - duplicate of the interface existing in the ag04-utils.jar

### Version 1.0.12

* Upgraded spring-boot to version 1.5.10.RELEASE
* Added JodaModulConfigurator with mappings for: LocalDate, LocalDateTime, DateTime joda data types

### Version 1.0.11

Improved ValidationError Dto:
* Added @JsonInclude(JsonInclude.Include.NON_NULL) annotation at class level
* Changed it so that it also extends BaseError (Dto)

### Version 1.0.10

* SKIPPED VERSION

### Version 1.0.9

* Added ValidationError DTO
* Upgraded spring-boot to version 1.5.9.RELEASE

### Version 1.0.8
Fixed bug in EntityConflictError DTO, invalid setting of entity property in setter method (by @jbrestovac);

### Version 1.0.7
Proširio EntityConflictError DTO s property-em: Object entity;

### Version 1.0.6

upgraded spring-boot to version 1.4.5.RELEASE and cleaned up unnecessary lib/jar version definitions so that now all dependencies are resolved through spring.


### Version 1.0.5

upgraded spring-boot to version 1.3.6.RELEASE and cleaned up unnecessary lib/jar version definitions so that now all dependencies are resolved through spring.

#### Dependencies:

* spring-boot :: 1.3.6.RELEASE
    * spring-boot-starter
    * spring-boot-starter-test
    * spring-jdbc
    * spring-webmvc
    * spring-test
    * spring-security-core
    * spring-security-web


* joda-time :: 2.8.2 (resolved through spring-boot-parent)


* com.fasterxml.jackson.core :: 2.6.7 (resolved through spring-boot-parent)
    * jackson-annotations
    * jackson-core
    * jackson-databind


* mockito-core :: 1.10.19 (resolved through spring-boot-parent)


* javax.servlet :: javax.servlet-api :: 3.0.1